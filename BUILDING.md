# Building StarKingdoms

StarKingdoms uses a small script called Spacetime as a build tool. Internally, it regenerates and runs a build.ninja file for compilation.

## Dependencies

For compilation, you will need the following tools installed:
- rust/cargo
- ninja
- inkscape
- sheep (see below)
- wasm-pack (see below - must be installed in a specific way)

`sheep` and `wasm-pack` can be installed with the build-command `install_tooling`. Other tools will need to be installed with your distribution's package manager.
`wasm-pack` *must* be installed with `install_tooling`, otherwise the build will crash.

If you get a `sheep: command not found` error, or the same with wasm-pack, add `$HOME/.cargo/bin/` to your `PATH`. 

## Running build commands

Build commands are ran with the `spacetime` shell script. Run `./spacetime <build-command>` to run a build.
`spacetime` can be run from any directory - you could run `../../spacetime run-server` from `server/src/`, and it would still properly build and run the server.

## Available build commands

Run the build-command `help` to view a list of all build-commands.