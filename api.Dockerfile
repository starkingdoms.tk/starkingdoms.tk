FROM ghcr.io/void-linux/void-linux:latest-full-x86_64

RUN xbps-install -S && xbps-install -f base-files && xbps-install -Syyu gcc

RUN mkdir /var/www

COPY target/release/starkingdoms-api /bin/starkingdoms-api
COPY api/static/ /var/www/static
COPY api/templates /var/www/templates

WORKDIR "/var/www"
CMD ["/bin/starkingdoms-api"]