use log::error;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};

pub static CONFIG: Lazy<StarkingdomsApiConfig> = Lazy::new(|| {
    let config_str = match fs::read_to_string("/etc/starkingdoms/config.toml") {
        Ok(str) => str,
        Err(e) => {
            error!("Unable to read config file: {}", e);
            std::process::exit(1);
        }
    };

    match toml::from_str(&config_str) {
        Ok(cfg) => cfg,
        Err(e) => {
            error!("Unable to parse config file: {}", e);
            std::process::exit(1);
        }
    }
});

#[derive(Serialize, Debug, Deserialize)]
pub struct StarkingdomsApiConfig {
    pub database: StarkingdomsApiConfigDatabase,
    pub server: StarkingdomsApiConfigServer,
    pub internal_tokens: Vec<String>,
    pub jwt_signing_secret: String,
    pub base: String,
    pub game: String,
    pub realms: HashMap<String, StarkingdomsApiConfigRealm>,
    pub servers: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct StarkingdomsApiConfigDatabase {
    pub url: String,
    #[serde(default = "max_connections_default")]
    pub max_connections: u32,
    #[serde(default = "min_connections_default")]
    pub min_connections: u32,
    #[serde(default = "time_defaults")]
    pub connect_timeout: u64,
    #[serde(default = "time_defaults")]
    pub acquire_timeout: u64,
    #[serde(default = "time_defaults")]
    pub idle_timeout: u64,
    #[serde(default = "time_defaults")]
    pub max_lifetime: u64,
    #[serde(default = "sqlx_logging_default")]
    pub sqlx_logging: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct StarkingdomsApiConfigServer {
    #[serde(default = "socketaddr_8080")]
    pub bind: SocketAddr,
}

/*
authorize-url = "https://api.e3t.cc/auth/discord/authorize.php"
public-key-url = "https://api.e3t.cc/auth/discord/public-key.txt"
issuer = "https://api.e3t.cc"
 */

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct StarkingdomsApiConfigRealm {
    pub authorize_url: String,
    pub public_key: String,
    pub issuer: String,
}

fn max_connections_default() -> u32 {
    100
}
fn min_connections_default() -> u32 {
    5
}
fn time_defaults() -> u64 {
    8
}
fn sqlx_logging_default() -> bool {
    true
}
fn socketaddr_8080() -> SocketAddr {
    SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::from([0, 0, 0, 0]), 8080))
}
