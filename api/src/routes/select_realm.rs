use crate::config::{StarkingdomsApiConfigRealm, CONFIG};
use crate::error::{APIError, APIErrorsResponse};
use crate::AppState;
use actix_web::web::Data;
use actix_web::{get, HttpResponse};
use log::error;
use serde::Serialize;
use std::collections::HashMap;
use tera::Context;

#[derive(Serialize)]
pub struct RealmsListTemplateContext {
    pub realms: HashMap<String, StarkingdomsApiConfigRealm>,
    pub back_to: String,
}

#[get("/select-realm")]
pub async fn select_realm(state: Data<AppState>) -> HttpResponse {
    let context = match Context::from_serialize(RealmsListTemplateContext {
        back_to: format!("{}/callback", CONFIG.base),
        realms: CONFIG.realms.clone(),
    }) {
        Ok(r) => r,
        Err(e) => {
            error!("[context] error creating render context: {}", e);
            return HttpResponse::InternalServerError().json(APIErrorsResponse {
                errors: vec![APIError {
                    code: "ERR_INTERNAL_SERVER_ERROR".to_string(),
                    message: "There was an error processing your request. Please try again later."
                        .to_string(),
                    path: None,
                }],
            });
        }
    };
    match state.templates.render("select_realm.tera", &context) {
        Ok(r) => HttpResponse::Ok().content_type("text/html").body(r),
        Err(e) => {
            error!("[context] error creating render context: {}", e);
            HttpResponse::InternalServerError().json(APIErrorsResponse {
                errors: vec![APIError {
                    code: "ERR_INTERNAL_SERVER_ERROR".to_string(),
                    message: "There was an error processing your request. Please try again later."
                        .to_string(),
                    path: None,
                }],
            })
        }
    }
}
