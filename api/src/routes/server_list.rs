use crate::config::CONFIG;
use actix_web::get;
use actix_web::web::Json;
use serde::Serialize;

#[derive(Serialize)]
pub struct ServerListResponse {
    pub servers: Vec<String>,
}

#[get("server-list")]
pub async fn server_list() -> Json<ServerListResponse> {
    Json(ServerListResponse {
        servers: CONFIG.servers.clone(),
    })
}
