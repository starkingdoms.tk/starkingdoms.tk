pub use sea_orm_migration::prelude::*;

pub mod m20230417_162824_create_table_users;
pub mod m20230417_164240_create_table_user_auth_realms;
pub mod m20230420_144333_create_table_user_data;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20230417_162824_create_table_users::Migration),
            Box::new(m20230417_164240_create_table_user_auth_realms::Migration),
            Box::new(m20230420_144333_create_table_user_data::Migration),
        ]
    }
}
