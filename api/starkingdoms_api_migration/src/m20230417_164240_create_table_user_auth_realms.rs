use crate::m20230417_162824_create_table_users::User;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(UserAuthRealm::Table)
                    .col(
                        ColumnDef::new(UserAuthRealm::Id)
                            .string()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(UserAuthRealm::Realm).string().not_null())
                    .col(
                        ColumnDef::new(UserAuthRealm::RealmNativeId)
                            .string()
                            .not_null(),
                    )
                    .col(ColumnDef::new(UserAuthRealm::User).string().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from(UserAuthRealm::Table, UserAuthRealm::User)
                            .to(User::Table, User::Id),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(UserAuthRealm::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
pub enum UserAuthRealm {
    Table,
    Id,
    Realm,
    RealmNativeId,
    User,
}
