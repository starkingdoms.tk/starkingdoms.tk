use crate::m20230417_164240_create_table_user_auth_realms::UserAuthRealm;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(UserSavefile::Table)
                    .col(
                        ColumnDef::new(UserSavefile::Id)
                            .string()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(UserSavefile::User).string().not_null())
                    .col(ColumnDef::new(UserSavefile::Data).string().not_null())
                    .col(
                        ColumnDef::new(UserSavefile::Timestamp)
                            .big_unsigned()
                            .not_null()
                            .unique_key(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(UserSavefile::Table, UserSavefile::User)
                            .to(UserAuthRealm::Table, UserAuthRealm::Id),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(UserSavefile::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
pub enum UserSavefile {
    Table,
    Id,
    User,
    Data,
    Timestamp,
}
