use sea_orm_migration::prelude::*;

#[async_std::main]
async fn main() {
    cli::run_cli(starkingdoms_api_migration::Migrator).await;
}
