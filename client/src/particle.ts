import {global} from "./index";

export interface Particle {
    x: number,
    y: number,
    lifetime: number,
    timer: number,
    startSize: number,
    finalSize: number,
    startRotation: number,
    finalRotation: number,
    startOpacity: number,
    endOpacity: number,
    startR: number,
    endR: number,
    startG: number,
    endG: number,
    startB: number,
    endB: number,
    velocity_x: number,
    velocity_y: number
}

let particles: Particle[] = [];

export function createParticle(particle: Particle) {
    particles.push(particle);
}

function lerp(start: number, end: number, time: number): number {
    return start + time * (end - start);
}

export function drawParticle(particle: Particle) {
    let t = particle.timer / particle.lifetime;
    let size = lerp(particle.startSize, particle.finalSize, t);
    let rotation = lerp(particle.startRotation, particle.finalRotation, t);

    global.context.save();

    global.context.translate(particle.x - global.me!.x, particle.y - global.me!.y);

    global.context.rotate(rotation);

    let opacity = Math.trunc(lerp(particle.startOpacity, particle.endOpacity, t));
    let r = Math.trunc(lerp(particle.startR, particle.endR, t));
    let g = Math.trunc(lerp(particle.startG, particle.endG, t));
    let b = Math.trunc(lerp(particle.startB, particle.endB, t));

    global.context.fillStyle = `rgb(${r} ${g} ${b} / ${opacity}%)`;
    global.context.fillRect(-size/2, -size/2, size, size);

    global.context.restore();
}

export function drawParticles() {
    for (let i = 0; i < particles.length; i++) {
        drawParticle(particles[i]);
    }
}

export function tickParticles(delta: number) {
    let keptParticles = []
    for (let i = 0; i < particles.length; i++) {
        particles[i].timer += delta;

        particles[i].x += particles[i].velocity_x * delta;
        particles[i].y += particles[i].velocity_y * delta;

        if (!(particles[i].timer > particles[i].lifetime)) {
            keptParticles.push(particles[i]);
        }
    }
    particles = keptParticles;
}

export function tickAndDrawParticles(delta: number) {
    tickParticles(delta);
    drawParticles();
}
