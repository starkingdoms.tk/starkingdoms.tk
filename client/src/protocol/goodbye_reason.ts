//@ts-nocheck

export const protobufPackage = "protocol.goodbye_reason";

export enum GoodbyeReason {
  UNKNOWN = 0,
  UnsupportedProtocol = 1,
  UnexpectedPacket = 2,
  UnexpectedNextState = 3,
  UsernameTaken = 4,
  PingPongTimeout = 5,
  Done = 6,
  UNRECOGNIZED = -1,
}

export function goodbyeReasonFromJSON(object: any): GoodbyeReason {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return GoodbyeReason.UNKNOWN;
    case 1:
    case "UnsupportedProtocol":
      return GoodbyeReason.UnsupportedProtocol;
    case 2:
    case "UnexpectedPacket":
      return GoodbyeReason.UnexpectedPacket;
    case 3:
    case "UnexpectedNextState":
      return GoodbyeReason.UnexpectedNextState;
    case 4:
    case "UsernameTaken":
      return GoodbyeReason.UsernameTaken;
    case 5:
    case "PingPongTimeout":
      return GoodbyeReason.PingPongTimeout;
    case 6:
    case "Done":
      return GoodbyeReason.Done;
    case -1:
    case "UNRECOGNIZED":
    default:
      return GoodbyeReason.UNRECOGNIZED;
  }
}

export function goodbyeReasonToJSON(object: GoodbyeReason): string {
  switch (object) {
    case GoodbyeReason.UNKNOWN:
      return "UNKNOWN";
    case GoodbyeReason.UnsupportedProtocol:
      return "UnsupportedProtocol";
    case GoodbyeReason.UnexpectedPacket:
      return "UnexpectedPacket";
    case GoodbyeReason.UnexpectedNextState:
      return "UnexpectedNextState";
    case GoodbyeReason.UsernameTaken:
      return "UsernameTaken";
    case GoodbyeReason.PingPongTimeout:
      return "PingPongTimeout";
    case GoodbyeReason.Done:
      return "Done";
    case GoodbyeReason.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}
