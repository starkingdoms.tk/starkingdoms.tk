//@ts-nocheck

export const protobufPackage = "protocol.input";

export enum InputType {
  UNKNOWN = 0,
  Left = 1,
  Middle = 2,
  Right = 3,
  UNRECOGNIZED = -1,
}

export function inputTypeFromJSON(object: any): InputType {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return InputType.UNKNOWN;
    case 1:
    case "Left":
      return InputType.Left;
    case 2:
    case "Middle":
      return InputType.Middle;
    case 3:
    case "Right":
      return InputType.Right;
    case -1:
    case "UNRECOGNIZED":
    default:
      return InputType.UNRECOGNIZED;
  }
}

export function inputTypeToJSON(object: InputType): string {
  switch (object) {
    case InputType.UNKNOWN:
      return "UNKNOWN";
    case InputType.Left:
      return "Left";
    case InputType.Middle:
      return "Middle";
    case InputType.Right:
      return "Right";
    case InputType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}
