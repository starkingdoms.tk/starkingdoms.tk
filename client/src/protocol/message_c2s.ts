//@ts-nocheck
import * as _m0 from "protobufjs/minimal";
import { GoodbyeReason, goodbyeReasonFromJSON, goodbyeReasonToJSON } from "./goodbye_reason";
import { InputType, inputTypeFromJSON, inputTypeToJSON } from "./input";
import { State, stateFromJSON, stateToJSON } from "./state";

export const protobufPackage = "protocol.message_c2s";

export interface MessageC2SHello {
  /** Version of the protocol. Currently always 1 */
  version: number;
  /** The username that the client is requesting. */
  requestedUsername: string;
  /** The state the connection will go into after the handshake. */
  nextState: State;
  token: string;
  user: string;
}

export enum MessageC2SHello_packetInfo {
  unknown = 0,
  type = 1,
  UNRECOGNIZED = -1,
}

export function messageC2SHello_packetInfoFromJSON(object: any): MessageC2SHello_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SHello_packetInfo.unknown;
    case 1:
    case "type":
      return MessageC2SHello_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SHello_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SHello_packetInfoToJSON(object: MessageC2SHello_packetInfo): string {
  switch (object) {
    case MessageC2SHello_packetInfo.unknown:
      return "unknown";
    case MessageC2SHello_packetInfo.type:
      return "type";
    case MessageC2SHello_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SGoodbye {
  /** The reason the client is disconnecting the server */
  reason: GoodbyeReason;
}

export enum MessageC2SGoodbye_packetInfo {
  unknown = 0,
  type = 2,
  UNRECOGNIZED = -1,
}

export function messageC2SGoodbye_packetInfoFromJSON(object: any): MessageC2SGoodbye_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SGoodbye_packetInfo.unknown;
    case 2:
    case "type":
      return MessageC2SGoodbye_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SGoodbye_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SGoodbye_packetInfoToJSON(object: MessageC2SGoodbye_packetInfo): string {
  switch (object) {
    case MessageC2SGoodbye_packetInfo.unknown:
      return "unknown";
    case MessageC2SGoodbye_packetInfo.type:
      return "type";
    case MessageC2SGoodbye_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SChat {
  /** The chat message to sent */
  message: string;
}

export enum MessageC2SChat_packetInfo {
  unknown = 0,
  type = 3,
  UNRECOGNIZED = -1,
}

export function messageC2SChat_packetInfoFromJSON(object: any): MessageC2SChat_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SChat_packetInfo.unknown;
    case 3:
    case "type":
      return MessageC2SChat_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SChat_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SChat_packetInfoToJSON(object: MessageC2SChat_packetInfo): string {
  switch (object) {
    case MessageC2SChat_packetInfo.unknown:
      return "unknown";
    case MessageC2SChat_packetInfo.type:
      return "type";
    case MessageC2SChat_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SPing {
}

export enum MessageC2SPing_packetInfo {
  unknown = 0,
  type = 4,
  UNRECOGNIZED = -1,
}

export function messageC2SPing_packetInfoFromJSON(object: any): MessageC2SPing_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SPing_packetInfo.unknown;
    case 4:
    case "type":
      return MessageC2SPing_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SPing_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SPing_packetInfoToJSON(object: MessageC2SPing_packetInfo): string {
  switch (object) {
    case MessageC2SPing_packetInfo.unknown:
      return "unknown";
    case MessageC2SPing_packetInfo.type:
      return "type";
    case MessageC2SPing_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SInput {
  upPressed: boolean;
  downPressed: boolean;
  leftPressed: boolean;
  rightPressed: boolean;
}

export enum MessageC2SInput_packetInfo {
  unknown = 0,
  type = 11,
  UNRECOGNIZED = -1,
}

export function messageC2SInput_packetInfoFromJSON(object: any): MessageC2SInput_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SInput_packetInfo.unknown;
    case 11:
    case "type":
      return MessageC2SInput_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SInput_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SInput_packetInfoToJSON(object: MessageC2SInput_packetInfo): string {
  switch (object) {
    case MessageC2SInput_packetInfo.unknown:
      return "unknown";
    case MessageC2SInput_packetInfo.type:
      return "type";
    case MessageC2SInput_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SAuthenticateAndBeamOut {
  /** The user ID that the client is authenticating as */
  userId: string;
  /** The token from the authentication server that the user is authenticating as */
  token: string;
}

export enum MessageC2SAuthenticateAndBeamOut_packetInfo {
  unknown = 0,
  type = 12,
  UNRECOGNIZED = -1,
}

export function messageC2SAuthenticateAndBeamOut_packetInfoFromJSON(
  object: any,
): MessageC2SAuthenticateAndBeamOut_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SAuthenticateAndBeamOut_packetInfo.unknown;
    case 12:
    case "type":
      return MessageC2SAuthenticateAndBeamOut_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SAuthenticateAndBeamOut_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SAuthenticateAndBeamOut_packetInfoToJSON(
  object: MessageC2SAuthenticateAndBeamOut_packetInfo,
): string {
  switch (object) {
    case MessageC2SAuthenticateAndBeamOut_packetInfo.unknown:
      return "unknown";
    case MessageC2SAuthenticateAndBeamOut_packetInfo.type:
      return "type";
    case MessageC2SAuthenticateAndBeamOut_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SMouseInput {
  worldposX: number;
  worldposY: number;
  button: InputType;
  released: boolean;
}

export enum MessageC2SMouseInput_packetInfo {
  unknown = 0,
  type = 13,
  UNRECOGNIZED = -1,
}

export function messageC2SMouseInput_packetInfoFromJSON(object: any): MessageC2SMouseInput_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SMouseInput_packetInfo.unknown;
    case 13:
    case "type":
      return MessageC2SMouseInput_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SMouseInput_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SMouseInput_packetInfoToJSON(object: MessageC2SMouseInput_packetInfo): string {
  switch (object) {
    case MessageC2SMouseInput_packetInfo.unknown:
      return "unknown";
    case MessageC2SMouseInput_packetInfo.type:
      return "type";
    case MessageC2SMouseInput_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SModuleGrabBegin {
  moduleId: number;
  worldposX: number;
  worldposY: number;
}

export enum MessageC2SModuleGrabBegin_packetInfo {
  unknown = 0,
  type = 14,
  UNRECOGNIZED = -1,
}

export function messageC2SModuleGrabBegin_packetInfoFromJSON(object: any): MessageC2SModuleGrabBegin_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SModuleGrabBegin_packetInfo.unknown;
    case 14:
    case "type":
      return MessageC2SModuleGrabBegin_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SModuleGrabBegin_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SModuleGrabBegin_packetInfoToJSON(object: MessageC2SModuleGrabBegin_packetInfo): string {
  switch (object) {
    case MessageC2SModuleGrabBegin_packetInfo.unknown:
      return "unknown";
    case MessageC2SModuleGrabBegin_packetInfo.type:
      return "type";
    case MessageC2SModuleGrabBegin_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SModuleGrabEnd {
  moduleId: number;
  worldposX: number;
  worldposY: number;
}

export enum MessageC2SModuleGrabEnd_packetInfo {
  unknown = 0,
  type = 15,
  UNRECOGNIZED = -1,
}

export function messageC2SModuleGrabEnd_packetInfoFromJSON(object: any): MessageC2SModuleGrabEnd_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SModuleGrabEnd_packetInfo.unknown;
    case 15:
    case "type":
      return MessageC2SModuleGrabEnd_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SModuleGrabEnd_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SModuleGrabEnd_packetInfoToJSON(object: MessageC2SModuleGrabEnd_packetInfo): string {
  switch (object) {
    case MessageC2SModuleGrabEnd_packetInfo.unknown:
      return "unknown";
    case MessageC2SModuleGrabEnd_packetInfo.type:
      return "type";
    case MessageC2SModuleGrabEnd_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageC2SModuleDetach {
  moduleId: number;
}

export enum MessageC2SModuleDetach_packetInfo {
  unknown = 0,
  type = 16,
  UNRECOGNIZED = -1,
}

export function messageC2SModuleDetach_packetInfoFromJSON(object: any): MessageC2SModuleDetach_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageC2SModuleDetach_packetInfo.unknown;
    case 16:
    case "type":
      return MessageC2SModuleDetach_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageC2SModuleDetach_packetInfo.UNRECOGNIZED;
  }
}

export function messageC2SModuleDetach_packetInfoToJSON(object: MessageC2SModuleDetach_packetInfo): string {
  switch (object) {
    case MessageC2SModuleDetach_packetInfo.unknown:
      return "unknown";
    case MessageC2SModuleDetach_packetInfo.type:
      return "type";
    case MessageC2SModuleDetach_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseMessageC2SHello(): MessageC2SHello {
  return { version: 0, requestedUsername: "", nextState: 0, token: "", user: "" };
}

export const MessageC2SHello = {
  encode(message: MessageC2SHello, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== 0) {
      writer.uint32(8).uint32(message.version);
    }
    if (message.requestedUsername !== "") {
      writer.uint32(18).string(message.requestedUsername);
    }
    if (message.nextState !== 0) {
      writer.uint32(24).int32(message.nextState);
    }
    if (message.token !== "") {
      writer.uint32(34).string(message.token);
    }
    if (message.user !== "") {
      writer.uint32(42).string(message.user);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SHello {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SHello();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.version = reader.uint32();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.requestedUsername = reader.string();
          continue;
        case 3:
          if (tag != 24) {
            break;
          }

          message.nextState = reader.int32() as any;
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.token = reader.string();
          continue;
        case 5:
          if (tag != 42) {
            break;
          }

          message.user = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SHello {
    return {
      version: isSet(object.version) ? Number(object.version) : 0,
      requestedUsername: isSet(object.requestedUsername) ? String(object.requestedUsername) : "",
      nextState: isSet(object.nextState) ? stateFromJSON(object.nextState) : 0,
      token: isSet(object.token) ? String(object.token) : "",
      user: isSet(object.user) ? String(object.user) : "",
    };
  },

  toJSON(message: MessageC2SHello): unknown {
    const obj: any = {};
    message.version !== undefined && (obj.version = Math.round(message.version));
    message.requestedUsername !== undefined && (obj.requestedUsername = message.requestedUsername);
    message.nextState !== undefined && (obj.nextState = stateToJSON(message.nextState));
    message.token !== undefined && (obj.token = message.token);
    message.user !== undefined && (obj.user = message.user);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SHello>, I>>(base?: I): MessageC2SHello {
    return MessageC2SHello.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SHello>, I>>(object: I): MessageC2SHello {
    const message = createBaseMessageC2SHello();
    message.version = object.version ?? 0;
    message.requestedUsername = object.requestedUsername ?? "";
    message.nextState = object.nextState ?? 0;
    message.token = object.token ?? "";
    message.user = object.user ?? "";
    return message;
  },
};

function createBaseMessageC2SGoodbye(): MessageC2SGoodbye {
  return { reason: 0 };
}

export const MessageC2SGoodbye = {
  encode(message: MessageC2SGoodbye, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.reason !== 0) {
      writer.uint32(8).int32(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SGoodbye {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SGoodbye();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.reason = reader.int32() as any;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SGoodbye {
    return { reason: isSet(object.reason) ? goodbyeReasonFromJSON(object.reason) : 0 };
  },

  toJSON(message: MessageC2SGoodbye): unknown {
    const obj: any = {};
    message.reason !== undefined && (obj.reason = goodbyeReasonToJSON(message.reason));
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SGoodbye>, I>>(base?: I): MessageC2SGoodbye {
    return MessageC2SGoodbye.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SGoodbye>, I>>(object: I): MessageC2SGoodbye {
    const message = createBaseMessageC2SGoodbye();
    message.reason = object.reason ?? 0;
    return message;
  },
};

function createBaseMessageC2SChat(): MessageC2SChat {
  return { message: "" };
}

export const MessageC2SChat = {
  encode(message: MessageC2SChat, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.message !== "") {
      writer.uint32(10).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SChat {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SChat();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.message = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SChat {
    return { message: isSet(object.message) ? String(object.message) : "" };
  },

  toJSON(message: MessageC2SChat): unknown {
    const obj: any = {};
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SChat>, I>>(base?: I): MessageC2SChat {
    return MessageC2SChat.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SChat>, I>>(object: I): MessageC2SChat {
    const message = createBaseMessageC2SChat();
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMessageC2SPing(): MessageC2SPing {
  return {};
}

export const MessageC2SPing = {
  encode(_: MessageC2SPing, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SPing {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SPing();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): MessageC2SPing {
    return {};
  },

  toJSON(_: MessageC2SPing): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SPing>, I>>(base?: I): MessageC2SPing {
    return MessageC2SPing.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SPing>, I>>(_: I): MessageC2SPing {
    const message = createBaseMessageC2SPing();
    return message;
  },
};

function createBaseMessageC2SInput(): MessageC2SInput {
  return { upPressed: false, downPressed: false, leftPressed: false, rightPressed: false };
}

export const MessageC2SInput = {
  encode(message: MessageC2SInput, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.upPressed === true) {
      writer.uint32(8).bool(message.upPressed);
    }
    if (message.downPressed === true) {
      writer.uint32(16).bool(message.downPressed);
    }
    if (message.leftPressed === true) {
      writer.uint32(24).bool(message.leftPressed);
    }
    if (message.rightPressed === true) {
      writer.uint32(32).bool(message.rightPressed);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SInput {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SInput();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.upPressed = reader.bool();
          continue;
        case 2:
          if (tag != 16) {
            break;
          }

          message.downPressed = reader.bool();
          continue;
        case 3:
          if (tag != 24) {
            break;
          }

          message.leftPressed = reader.bool();
          continue;
        case 4:
          if (tag != 32) {
            break;
          }

          message.rightPressed = reader.bool();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SInput {
    return {
      upPressed: isSet(object.upPressed) ? Boolean(object.upPressed) : false,
      downPressed: isSet(object.downPressed) ? Boolean(object.downPressed) : false,
      leftPressed: isSet(object.leftPressed) ? Boolean(object.leftPressed) : false,
      rightPressed: isSet(object.rightPressed) ? Boolean(object.rightPressed) : false,
    };
  },

  toJSON(message: MessageC2SInput): unknown {
    const obj: any = {};
    message.upPressed !== undefined && (obj.upPressed = message.upPressed);
    message.downPressed !== undefined && (obj.downPressed = message.downPressed);
    message.leftPressed !== undefined && (obj.leftPressed = message.leftPressed);
    message.rightPressed !== undefined && (obj.rightPressed = message.rightPressed);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SInput>, I>>(base?: I): MessageC2SInput {
    return MessageC2SInput.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SInput>, I>>(object: I): MessageC2SInput {
    const message = createBaseMessageC2SInput();
    message.upPressed = object.upPressed ?? false;
    message.downPressed = object.downPressed ?? false;
    message.leftPressed = object.leftPressed ?? false;
    message.rightPressed = object.rightPressed ?? false;
    return message;
  },
};

function createBaseMessageC2SAuthenticateAndBeamOut(): MessageC2SAuthenticateAndBeamOut {
  return { userId: "", token: "" };
}

export const MessageC2SAuthenticateAndBeamOut = {
  encode(message: MessageC2SAuthenticateAndBeamOut, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.userId !== "") {
      writer.uint32(10).string(message.userId);
    }
    if (message.token !== "") {
      writer.uint32(18).string(message.token);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SAuthenticateAndBeamOut {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SAuthenticateAndBeamOut();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.userId = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.token = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SAuthenticateAndBeamOut {
    return {
      userId: isSet(object.userId) ? String(object.userId) : "",
      token: isSet(object.token) ? String(object.token) : "",
    };
  },

  toJSON(message: MessageC2SAuthenticateAndBeamOut): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = message.userId);
    message.token !== undefined && (obj.token = message.token);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SAuthenticateAndBeamOut>, I>>(
    base?: I,
  ): MessageC2SAuthenticateAndBeamOut {
    return MessageC2SAuthenticateAndBeamOut.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SAuthenticateAndBeamOut>, I>>(
    object: I,
  ): MessageC2SAuthenticateAndBeamOut {
    const message = createBaseMessageC2SAuthenticateAndBeamOut();
    message.userId = object.userId ?? "";
    message.token = object.token ?? "";
    return message;
  },
};

function createBaseMessageC2SMouseInput(): MessageC2SMouseInput {
  return { worldposX: 0, worldposY: 0, button: 0, released: false };
}

export const MessageC2SMouseInput = {
  encode(message: MessageC2SMouseInput, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.worldposX !== 0) {
      writer.uint32(9).double(message.worldposX);
    }
    if (message.worldposY !== 0) {
      writer.uint32(17).double(message.worldposY);
    }
    if (message.button !== 0) {
      writer.uint32(24).int32(message.button);
    }
    if (message.released === true) {
      writer.uint32(32).bool(message.released);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SMouseInput {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SMouseInput();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 9) {
            break;
          }

          message.worldposX = reader.double();
          continue;
        case 2:
          if (tag != 17) {
            break;
          }

          message.worldposY = reader.double();
          continue;
        case 3:
          if (tag != 24) {
            break;
          }

          message.button = reader.int32() as any;
          continue;
        case 4:
          if (tag != 32) {
            break;
          }

          message.released = reader.bool();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SMouseInput {
    return {
      worldposX: isSet(object.worldposX) ? Number(object.worldposX) : 0,
      worldposY: isSet(object.worldposY) ? Number(object.worldposY) : 0,
      button: isSet(object.button) ? inputTypeFromJSON(object.button) : 0,
      released: isSet(object.released) ? Boolean(object.released) : false,
    };
  },

  toJSON(message: MessageC2SMouseInput): unknown {
    const obj: any = {};
    message.worldposX !== undefined && (obj.worldposX = message.worldposX);
    message.worldposY !== undefined && (obj.worldposY = message.worldposY);
    message.button !== undefined && (obj.button = inputTypeToJSON(message.button));
    message.released !== undefined && (obj.released = message.released);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SMouseInput>, I>>(base?: I): MessageC2SMouseInput {
    return MessageC2SMouseInput.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SMouseInput>, I>>(object: I): MessageC2SMouseInput {
    const message = createBaseMessageC2SMouseInput();
    message.worldposX = object.worldposX ?? 0;
    message.worldposY = object.worldposY ?? 0;
    message.button = object.button ?? 0;
    message.released = object.released ?? false;
    return message;
  },
};

function createBaseMessageC2SModuleGrabBegin(): MessageC2SModuleGrabBegin {
  return { moduleId: 0, worldposX: 0, worldposY: 0 };
}

export const MessageC2SModuleGrabBegin = {
  encode(message: MessageC2SModuleGrabBegin, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.moduleId !== 0) {
      writer.uint32(8).uint32(message.moduleId);
    }
    if (message.worldposX !== 0) {
      writer.uint32(17).double(message.worldposX);
    }
    if (message.worldposY !== 0) {
      writer.uint32(25).double(message.worldposY);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SModuleGrabBegin {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SModuleGrabBegin();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.moduleId = reader.uint32();
          continue;
        case 2:
          if (tag != 17) {
            break;
          }

          message.worldposX = reader.double();
          continue;
        case 3:
          if (tag != 25) {
            break;
          }

          message.worldposY = reader.double();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SModuleGrabBegin {
    return {
      moduleId: isSet(object.moduleId) ? Number(object.moduleId) : 0,
      worldposX: isSet(object.worldposX) ? Number(object.worldposX) : 0,
      worldposY: isSet(object.worldposY) ? Number(object.worldposY) : 0,
    };
  },

  toJSON(message: MessageC2SModuleGrabBegin): unknown {
    const obj: any = {};
    message.moduleId !== undefined && (obj.moduleId = Math.round(message.moduleId));
    message.worldposX !== undefined && (obj.worldposX = message.worldposX);
    message.worldposY !== undefined && (obj.worldposY = message.worldposY);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SModuleGrabBegin>, I>>(base?: I): MessageC2SModuleGrabBegin {
    return MessageC2SModuleGrabBegin.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SModuleGrabBegin>, I>>(object: I): MessageC2SModuleGrabBegin {
    const message = createBaseMessageC2SModuleGrabBegin();
    message.moduleId = object.moduleId ?? 0;
    message.worldposX = object.worldposX ?? 0;
    message.worldposY = object.worldposY ?? 0;
    return message;
  },
};

function createBaseMessageC2SModuleGrabEnd(): MessageC2SModuleGrabEnd {
  return { moduleId: 0, worldposX: 0, worldposY: 0 };
}

export const MessageC2SModuleGrabEnd = {
  encode(message: MessageC2SModuleGrabEnd, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.moduleId !== 0) {
      writer.uint32(8).uint32(message.moduleId);
    }
    if (message.worldposX !== 0) {
      writer.uint32(17).double(message.worldposX);
    }
    if (message.worldposY !== 0) {
      writer.uint32(25).double(message.worldposY);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SModuleGrabEnd {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SModuleGrabEnd();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.moduleId = reader.uint32();
          continue;
        case 2:
          if (tag != 17) {
            break;
          }

          message.worldposX = reader.double();
          continue;
        case 3:
          if (tag != 25) {
            break;
          }

          message.worldposY = reader.double();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SModuleGrabEnd {
    return {
      moduleId: isSet(object.moduleId) ? Number(object.moduleId) : 0,
      worldposX: isSet(object.worldposX) ? Number(object.worldposX) : 0,
      worldposY: isSet(object.worldposY) ? Number(object.worldposY) : 0,
    };
  },

  toJSON(message: MessageC2SModuleGrabEnd): unknown {
    const obj: any = {};
    message.moduleId !== undefined && (obj.moduleId = Math.round(message.moduleId));
    message.worldposX !== undefined && (obj.worldposX = message.worldposX);
    message.worldposY !== undefined && (obj.worldposY = message.worldposY);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SModuleGrabEnd>, I>>(base?: I): MessageC2SModuleGrabEnd {
    return MessageC2SModuleGrabEnd.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SModuleGrabEnd>, I>>(object: I): MessageC2SModuleGrabEnd {
    const message = createBaseMessageC2SModuleGrabEnd();
    message.moduleId = object.moduleId ?? 0;
    message.worldposX = object.worldposX ?? 0;
    message.worldposY = object.worldposY ?? 0;
    return message;
  },
};

function createBaseMessageC2SModuleDetach(): MessageC2SModuleDetach {
  return { moduleId: 0 };
}

export const MessageC2SModuleDetach = {
  encode(message: MessageC2SModuleDetach, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.moduleId !== 0) {
      writer.uint32(8).uint32(message.moduleId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageC2SModuleDetach {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageC2SModuleDetach();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.moduleId = reader.uint32();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageC2SModuleDetach {
    return { moduleId: isSet(object.moduleId) ? Number(object.moduleId) : 0 };
  },

  toJSON(message: MessageC2SModuleDetach): unknown {
    const obj: any = {};
    message.moduleId !== undefined && (obj.moduleId = Math.round(message.moduleId));
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageC2SModuleDetach>, I>>(base?: I): MessageC2SModuleDetach {
    return MessageC2SModuleDetach.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageC2SModuleDetach>, I>>(object: I): MessageC2SModuleDetach {
    const message = createBaseMessageC2SModuleDetach();
    message.moduleId = object.moduleId ?? 0;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
