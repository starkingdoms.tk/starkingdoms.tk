//@ts-nocheck
import * as _m0 from "protobufjs/minimal";
import { GoodbyeReason, goodbyeReasonFromJSON, goodbyeReasonToJSON } from "./goodbye_reason";
import { AttachedModule, Module } from "./module";
import { Planet } from "./planet";
import { Player } from "./player";
import { State, stateFromJSON, stateToJSON } from "./state";

export const protobufPackage = "protocol.message_s2c";

export interface MessageS2CHello {
  /** The version of the protocol in use. Currently always 1. */
  version: number;
  /** The username actually assigned to the player */
  givenUsername: string;
  /** The state to switch the game into */
  nextState: State;
}

export enum MessageS2CHello_packetInfo {
  unknown = 0,
  type = 5,
  UNRECOGNIZED = -1,
}

export function messageS2CHello_packetInfoFromJSON(object: any): MessageS2CHello_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CHello_packetInfo.unknown;
    case 5:
    case "type":
      return MessageS2CHello_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CHello_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CHello_packetInfoToJSON(object: MessageS2CHello_packetInfo): string {
  switch (object) {
    case MessageS2CHello_packetInfo.unknown:
      return "unknown";
    case MessageS2CHello_packetInfo.type:
      return "type";
    case MessageS2CHello_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CGoodbye {
  /** The reason for the disconnect */
  reason: GoodbyeReason;
}

export enum MessageS2CGoodbye_packetInfo {
  unknown = 0,
  type = 6,
  UNRECOGNIZED = -1,
}

export function messageS2CGoodbye_packetInfoFromJSON(object: any): MessageS2CGoodbye_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CGoodbye_packetInfo.unknown;
    case 6:
    case "type":
      return MessageS2CGoodbye_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CGoodbye_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CGoodbye_packetInfoToJSON(object: MessageS2CGoodbye_packetInfo): string {
  switch (object) {
    case MessageS2CGoodbye_packetInfo.unknown:
      return "unknown";
    case MessageS2CGoodbye_packetInfo.type:
      return "type";
    case MessageS2CGoodbye_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CChat {
  /** The username of the player who sent the message */
  from: string;
  /** The contents of the chat message */
  message: string;
}

export enum MessageS2CChat_packetInfo {
  unknown = 0,
  type = 7,
  UNRECOGNIZED = -1,
}

export function messageS2CChat_packetInfoFromJSON(object: any): MessageS2CChat_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CChat_packetInfo.unknown;
    case 7:
    case "type":
      return MessageS2CChat_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CChat_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CChat_packetInfoToJSON(object: MessageS2CChat_packetInfo): string {
  switch (object) {
    case MessageS2CChat_packetInfo.unknown:
      return "unknown";
    case MessageS2CChat_packetInfo.type:
      return "type";
    case MessageS2CChat_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CPong {
}

export enum MessageS2CPong_packetInfo {
  unknown = 0,
  type = 8,
  UNRECOGNIZED = -1,
}

export function messageS2CPong_packetInfoFromJSON(object: any): MessageS2CPong_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CPong_packetInfo.unknown;
    case 8:
    case "type":
      return MessageS2CPong_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CPong_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CPong_packetInfoToJSON(object: MessageS2CPong_packetInfo): string {
  switch (object) {
    case MessageS2CPong_packetInfo.unknown:
      return "unknown";
    case MessageS2CPong_packetInfo.type:
      return "type";
    case MessageS2CPong_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CPlayersUpdate {
  /** List of all players in the server */
  players: Player[];
}

export enum MessageS2CPlayersUpdate_packetInfo {
  unknown = 0,
  type = 9,
  UNRECOGNIZED = -1,
}

export function messageS2CPlayersUpdate_packetInfoFromJSON(object: any): MessageS2CPlayersUpdate_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CPlayersUpdate_packetInfo.unknown;
    case 9:
    case "type":
      return MessageS2CPlayersUpdate_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CPlayersUpdate_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CPlayersUpdate_packetInfoToJSON(object: MessageS2CPlayersUpdate_packetInfo): string {
  switch (object) {
    case MessageS2CPlayersUpdate_packetInfo.unknown:
      return "unknown";
    case MessageS2CPlayersUpdate_packetInfo.type:
      return "type";
    case MessageS2CPlayersUpdate_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CPlanetData {
  /** List of all planets on the server */
  planets: Planet[];
}

export enum MessageS2CPlanetData_packetInfo {
  unknown = 0,
  type = 10,
  UNRECOGNIZED = -1,
}

export function messageS2CPlanetData_packetInfoFromJSON(object: any): MessageS2CPlanetData_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CPlanetData_packetInfo.unknown;
    case 10:
    case "type":
      return MessageS2CPlanetData_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CPlanetData_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CPlanetData_packetInfoToJSON(object: MessageS2CPlanetData_packetInfo): string {
  switch (object) {
    case MessageS2CPlanetData_packetInfo.unknown:
      return "unknown";
    case MessageS2CPlanetData_packetInfo.type:
      return "type";
    case MessageS2CPlanetData_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CModulesUpdate {
  modules: Module[];
}

export enum MessageS2CModulesUpdate_packetInfo {
  unknown = 0,
  type = 11,
  UNRECOGNIZED = -1,
}

export function messageS2CModulesUpdate_packetInfoFromJSON(object: any): MessageS2CModulesUpdate_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CModulesUpdate_packetInfo.unknown;
    case 11:
    case "type":
      return MessageS2CModulesUpdate_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CModulesUpdate_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CModulesUpdate_packetInfoToJSON(object: MessageS2CModulesUpdate_packetInfo): string {
  switch (object) {
    case MessageS2CModulesUpdate_packetInfo.unknown:
      return "unknown";
    case MessageS2CModulesUpdate_packetInfo.type:
      return "type";
    case MessageS2CModulesUpdate_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CModuleTreeUpdate {
  tree: AttachedModule[];
}

export enum MessageS2CModuleTreeUpdate_packetInfo {
  unknown = 0,
  type = 12,
  UNRECOGNIZED = -1,
}

export function messageS2CModuleTreeUpdate_packetInfoFromJSON(object: any): MessageS2CModuleTreeUpdate_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CModuleTreeUpdate_packetInfo.unknown;
    case 12:
    case "type":
      return MessageS2CModuleTreeUpdate_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CModuleTreeUpdate_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CModuleTreeUpdate_packetInfoToJSON(object: MessageS2CModuleTreeUpdate_packetInfo): string {
  switch (object) {
    case MessageS2CModuleTreeUpdate_packetInfo.unknown:
      return "unknown";
    case MessageS2CModuleTreeUpdate_packetInfo.type:
      return "type";
    case MessageS2CModuleTreeUpdate_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CModuleAdd {
  module: AttachedModule | undefined;
}

export enum MessageS2CModuleAdd_packetInfo {
  unknown = 0,
  type = 13,
  UNRECOGNIZED = -1,
}

export function messageS2CModuleAdd_packetInfoFromJSON(object: any): MessageS2CModuleAdd_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CModuleAdd_packetInfo.unknown;
    case 13:
    case "type":
      return MessageS2CModuleAdd_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CModuleAdd_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CModuleAdd_packetInfoToJSON(object: MessageS2CModuleAdd_packetInfo): string {
  switch (object) {
    case MessageS2CModuleAdd_packetInfo.unknown:
      return "unknown";
    case MessageS2CModuleAdd_packetInfo.type:
      return "type";
    case MessageS2CModuleAdd_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface MessageS2CModuleRemove {
  module: Module | undefined;
}

export enum MessageS2CModuleRemove_packetInfo {
  unknown = 0,
  type = 14,
  UNRECOGNIZED = -1,
}

export function messageS2CModuleRemove_packetInfoFromJSON(object: any): MessageS2CModuleRemove_packetInfo {
  switch (object) {
    case 0:
    case "unknown":
      return MessageS2CModuleRemove_packetInfo.unknown;
    case 14:
    case "type":
      return MessageS2CModuleRemove_packetInfo.type;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MessageS2CModuleRemove_packetInfo.UNRECOGNIZED;
  }
}

export function messageS2CModuleRemove_packetInfoToJSON(object: MessageS2CModuleRemove_packetInfo): string {
  switch (object) {
    case MessageS2CModuleRemove_packetInfo.unknown:
      return "unknown";
    case MessageS2CModuleRemove_packetInfo.type:
      return "type";
    case MessageS2CModuleRemove_packetInfo.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseMessageS2CHello(): MessageS2CHello {
  return { version: 0, givenUsername: "", nextState: 0 };
}

export const MessageS2CHello = {
  encode(message: MessageS2CHello, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== 0) {
      writer.uint32(8).uint32(message.version);
    }
    if (message.givenUsername !== "") {
      writer.uint32(18).string(message.givenUsername);
    }
    if (message.nextState !== 0) {
      writer.uint32(24).int32(message.nextState);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CHello {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CHello();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.version = reader.uint32();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.givenUsername = reader.string();
          continue;
        case 3:
          if (tag != 24) {
            break;
          }

          message.nextState = reader.int32() as any;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CHello {
    return {
      version: isSet(object.version) ? Number(object.version) : 0,
      givenUsername: isSet(object.givenUsername) ? String(object.givenUsername) : "",
      nextState: isSet(object.nextState) ? stateFromJSON(object.nextState) : 0,
    };
  },

  toJSON(message: MessageS2CHello): unknown {
    const obj: any = {};
    message.version !== undefined && (obj.version = Math.round(message.version));
    message.givenUsername !== undefined && (obj.givenUsername = message.givenUsername);
    message.nextState !== undefined && (obj.nextState = stateToJSON(message.nextState));
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CHello>, I>>(base?: I): MessageS2CHello {
    return MessageS2CHello.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CHello>, I>>(object: I): MessageS2CHello {
    const message = createBaseMessageS2CHello();
    message.version = object.version ?? 0;
    message.givenUsername = object.givenUsername ?? "";
    message.nextState = object.nextState ?? 0;
    return message;
  },
};

function createBaseMessageS2CGoodbye(): MessageS2CGoodbye {
  return { reason: 0 };
}

export const MessageS2CGoodbye = {
  encode(message: MessageS2CGoodbye, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.reason !== 0) {
      writer.uint32(8).int32(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CGoodbye {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CGoodbye();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.reason = reader.int32() as any;
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CGoodbye {
    return { reason: isSet(object.reason) ? goodbyeReasonFromJSON(object.reason) : 0 };
  },

  toJSON(message: MessageS2CGoodbye): unknown {
    const obj: any = {};
    message.reason !== undefined && (obj.reason = goodbyeReasonToJSON(message.reason));
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CGoodbye>, I>>(base?: I): MessageS2CGoodbye {
    return MessageS2CGoodbye.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CGoodbye>, I>>(object: I): MessageS2CGoodbye {
    const message = createBaseMessageS2CGoodbye();
    message.reason = object.reason ?? 0;
    return message;
  },
};

function createBaseMessageS2CChat(): MessageS2CChat {
  return { from: "", message: "" };
}

export const MessageS2CChat = {
  encode(message: MessageS2CChat, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.from !== "") {
      writer.uint32(10).string(message.from);
    }
    if (message.message !== "") {
      writer.uint32(18).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CChat {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CChat();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.from = reader.string();
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.message = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CChat {
    return {
      from: isSet(object.from) ? String(object.from) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MessageS2CChat): unknown {
    const obj: any = {};
    message.from !== undefined && (obj.from = message.from);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CChat>, I>>(base?: I): MessageS2CChat {
    return MessageS2CChat.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CChat>, I>>(object: I): MessageS2CChat {
    const message = createBaseMessageS2CChat();
    message.from = object.from ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMessageS2CPong(): MessageS2CPong {
  return {};
}

export const MessageS2CPong = {
  encode(_: MessageS2CPong, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CPong {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CPong();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): MessageS2CPong {
    return {};
  },

  toJSON(_: MessageS2CPong): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CPong>, I>>(base?: I): MessageS2CPong {
    return MessageS2CPong.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CPong>, I>>(_: I): MessageS2CPong {
    const message = createBaseMessageS2CPong();
    return message;
  },
};

function createBaseMessageS2CPlayersUpdate(): MessageS2CPlayersUpdate {
  return { players: [] };
}

export const MessageS2CPlayersUpdate = {
  encode(message: MessageS2CPlayersUpdate, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.players) {
      Player.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CPlayersUpdate {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CPlayersUpdate();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.players.push(Player.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CPlayersUpdate {
    return { players: Array.isArray(object?.players) ? object.players.map((e: any) => Player.fromJSON(e)) : [] };
  },

  toJSON(message: MessageS2CPlayersUpdate): unknown {
    const obj: any = {};
    if (message.players) {
      obj.players = message.players.map((e) => e ? Player.toJSON(e) : undefined);
    } else {
      obj.players = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CPlayersUpdate>, I>>(base?: I): MessageS2CPlayersUpdate {
    return MessageS2CPlayersUpdate.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CPlayersUpdate>, I>>(object: I): MessageS2CPlayersUpdate {
    const message = createBaseMessageS2CPlayersUpdate();
    message.players = object.players?.map((e) => Player.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMessageS2CPlanetData(): MessageS2CPlanetData {
  return { planets: [] };
}

export const MessageS2CPlanetData = {
  encode(message: MessageS2CPlanetData, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.planets) {
      Planet.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CPlanetData {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CPlanetData();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.planets.push(Planet.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CPlanetData {
    return { planets: Array.isArray(object?.planets) ? object.planets.map((e: any) => Planet.fromJSON(e)) : [] };
  },

  toJSON(message: MessageS2CPlanetData): unknown {
    const obj: any = {};
    if (message.planets) {
      obj.planets = message.planets.map((e) => e ? Planet.toJSON(e) : undefined);
    } else {
      obj.planets = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CPlanetData>, I>>(base?: I): MessageS2CPlanetData {
    return MessageS2CPlanetData.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CPlanetData>, I>>(object: I): MessageS2CPlanetData {
    const message = createBaseMessageS2CPlanetData();
    message.planets = object.planets?.map((e) => Planet.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMessageS2CModulesUpdate(): MessageS2CModulesUpdate {
  return { modules: [] };
}

export const MessageS2CModulesUpdate = {
  encode(message: MessageS2CModulesUpdate, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.modules) {
      Module.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CModulesUpdate {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CModulesUpdate();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.modules.push(Module.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CModulesUpdate {
    return { modules: Array.isArray(object?.modules) ? object.modules.map((e: any) => Module.fromJSON(e)) : [] };
  },

  toJSON(message: MessageS2CModulesUpdate): unknown {
    const obj: any = {};
    if (message.modules) {
      obj.modules = message.modules.map((e) => e ? Module.toJSON(e) : undefined);
    } else {
      obj.modules = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CModulesUpdate>, I>>(base?: I): MessageS2CModulesUpdate {
    return MessageS2CModulesUpdate.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CModulesUpdate>, I>>(object: I): MessageS2CModulesUpdate {
    const message = createBaseMessageS2CModulesUpdate();
    message.modules = object.modules?.map((e) => Module.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMessageS2CModuleTreeUpdate(): MessageS2CModuleTreeUpdate {
  return { tree: [] };
}

export const MessageS2CModuleTreeUpdate = {
  encode(message: MessageS2CModuleTreeUpdate, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tree) {
      AttachedModule.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CModuleTreeUpdate {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CModuleTreeUpdate();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.tree.push(AttachedModule.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CModuleTreeUpdate {
    return { tree: Array.isArray(object?.tree) ? object.tree.map((e: any) => AttachedModule.fromJSON(e)) : [] };
  },

  toJSON(message: MessageS2CModuleTreeUpdate): unknown {
    const obj: any = {};
    if (message.tree) {
      obj.tree = message.tree.map((e) => e ? AttachedModule.toJSON(e) : undefined);
    } else {
      obj.tree = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CModuleTreeUpdate>, I>>(base?: I): MessageS2CModuleTreeUpdate {
    return MessageS2CModuleTreeUpdate.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CModuleTreeUpdate>, I>>(object: I): MessageS2CModuleTreeUpdate {
    const message = createBaseMessageS2CModuleTreeUpdate();
    message.tree = object.tree?.map((e) => AttachedModule.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMessageS2CModuleAdd(): MessageS2CModuleAdd {
  return { module: undefined };
}

export const MessageS2CModuleAdd = {
  encode(message: MessageS2CModuleAdd, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.module !== undefined) {
      AttachedModule.encode(message.module, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CModuleAdd {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CModuleAdd();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.module = AttachedModule.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CModuleAdd {
    return { module: isSet(object.module) ? AttachedModule.fromJSON(object.module) : undefined };
  },

  toJSON(message: MessageS2CModuleAdd): unknown {
    const obj: any = {};
    message.module !== undefined && (obj.module = message.module ? AttachedModule.toJSON(message.module) : undefined);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CModuleAdd>, I>>(base?: I): MessageS2CModuleAdd {
    return MessageS2CModuleAdd.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CModuleAdd>, I>>(object: I): MessageS2CModuleAdd {
    const message = createBaseMessageS2CModuleAdd();
    message.module = (object.module !== undefined && object.module !== null)
      ? AttachedModule.fromPartial(object.module)
      : undefined;
    return message;
  },
};

function createBaseMessageS2CModuleRemove(): MessageS2CModuleRemove {
  return { module: undefined };
}

export const MessageS2CModuleRemove = {
  encode(message: MessageS2CModuleRemove, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.module !== undefined) {
      Module.encode(message.module, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MessageS2CModuleRemove {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMessageS2CModuleRemove();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 10) {
            break;
          }

          message.module = Module.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MessageS2CModuleRemove {
    return { module: isSet(object.module) ? Module.fromJSON(object.module) : undefined };
  },

  toJSON(message: MessageS2CModuleRemove): unknown {
    const obj: any = {};
    message.module !== undefined && (obj.module = message.module ? Module.toJSON(message.module) : undefined);
    return obj;
  },

  create<I extends Exact<DeepPartial<MessageS2CModuleRemove>, I>>(base?: I): MessageS2CModuleRemove {
    return MessageS2CModuleRemove.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MessageS2CModuleRemove>, I>>(object: I): MessageS2CModuleRemove {
    const message = createBaseMessageS2CModuleRemove();
    message.module = (object.module !== undefined && object.module !== null)
      ? Module.fromPartial(object.module)
      : undefined;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
