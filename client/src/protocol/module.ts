//@ts-nocheck
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "protocol.module";

export enum ModuleType {
  UNKNOWN = 0,
  Cargo = 1,
  LandingThruster = 2,
  LandingThrusterSuspension = 3,
  Hub = 4,
  UNRECOGNIZED = -1,
}

export function moduleTypeFromJSON(object: any): ModuleType {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return ModuleType.UNKNOWN;
    case 1:
    case "Cargo":
      return ModuleType.Cargo;
    case 2:
    case "LandingThruster":
      return ModuleType.LandingThruster;
    case 3:
    case "LandingThrusterSuspension":
      return ModuleType.LandingThrusterSuspension;
    case 4:
    case "Hub":
      return ModuleType.Hub;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ModuleType.UNRECOGNIZED;
  }
}

export function moduleTypeToJSON(object: ModuleType): string {
  switch (object) {
    case ModuleType.UNKNOWN:
      return "UNKNOWN";
    case ModuleType.Cargo:
      return "Cargo";
    case ModuleType.LandingThruster:
      return "LandingThruster";
    case ModuleType.LandingThrusterSuspension:
      return "LandingThrusterSuspension";
    case ModuleType.Hub:
      return "Hub";
    case ModuleType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface Module {
  moduleType: ModuleType;
  rotation: number;
  x: number;
  y: number;
  id: number;
  flags: number;
  children: Attachment[];
}

export interface AttachedModule {
  moduleType: ModuleType;
  rotation: number;
  x: number;
  y: number;
  id: number;
  children: Attachment[];
}

export interface Attachment {
  id: number;
  slot: number;
}

function createBaseModule(): Module {
  return { moduleType: 0, rotation: 0, x: 0, y: 0, id: 0, flags: 0, children: [] };
}

export const Module = {
  encode(message: Module, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.moduleType !== 0) {
      writer.uint32(8).int32(message.moduleType);
    }
    if (message.rotation !== 0) {
      writer.uint32(17).double(message.rotation);
    }
    if (message.x !== 0) {
      writer.uint32(25).double(message.x);
    }
    if (message.y !== 0) {
      writer.uint32(33).double(message.y);
    }
    if (message.id !== 0) {
      writer.uint32(40).uint32(message.id);
    }
    if (message.flags !== 0) {
      writer.uint32(48).uint32(message.flags);
    }
    for (const v of message.children) {
      Attachment.encode(v!, writer.uint32(58).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Module {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseModule();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.moduleType = reader.int32() as any;
          continue;
        case 2:
          if (tag != 17) {
            break;
          }

          message.rotation = reader.double();
          continue;
        case 3:
          if (tag != 25) {
            break;
          }

          message.x = reader.double();
          continue;
        case 4:
          if (tag != 33) {
            break;
          }

          message.y = reader.double();
          continue;
        case 5:
          if (tag != 40) {
            break;
          }

          message.id = reader.uint32();
          continue;
        case 6:
          if (tag != 48) {
            break;
          }

          message.flags = reader.uint32();
          continue;
        case 7:
          if (tag != 58) {
            break;
          }

          message.children.push(Attachment.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Module {
    return {
      moduleType: isSet(object.moduleType) ? moduleTypeFromJSON(object.moduleType) : 0,
      rotation: isSet(object.rotation) ? Number(object.rotation) : 0,
      x: isSet(object.x) ? Number(object.x) : 0,
      y: isSet(object.y) ? Number(object.y) : 0,
      id: isSet(object.id) ? Number(object.id) : 0,
      flags: isSet(object.flags) ? Number(object.flags) : 0,
      children: Array.isArray(object?.children) ? object.children.map((e: any) => Attachment.fromJSON(e)) : [],
    };
  },

  toJSON(message: Module): unknown {
    const obj: any = {};
    message.moduleType !== undefined && (obj.moduleType = moduleTypeToJSON(message.moduleType));
    message.rotation !== undefined && (obj.rotation = message.rotation);
    message.x !== undefined && (obj.x = message.x);
    message.y !== undefined && (obj.y = message.y);
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.flags !== undefined && (obj.flags = Math.round(message.flags));
    if (message.children) {
      obj.children = message.children.map((e) => e ? Attachment.toJSON(e) : undefined);
    } else {
      obj.children = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<Module>, I>>(base?: I): Module {
    return Module.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Module>, I>>(object: I): Module {
    const message = createBaseModule();
    message.moduleType = object.moduleType ?? 0;
    message.rotation = object.rotation ?? 0;
    message.x = object.x ?? 0;
    message.y = object.y ?? 0;
    message.id = object.id ?? 0;
    message.flags = object.flags ?? 0;
    message.children = object.children?.map((e) => Attachment.fromPartial(e)) || [];
    return message;
  },
};

function createBaseAttachedModule(): AttachedModule {
  return { moduleType: 0, rotation: 0, x: 0, y: 0, id: 0, children: [] };
}

export const AttachedModule = {
  encode(message: AttachedModule, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.moduleType !== 0) {
      writer.uint32(8).int32(message.moduleType);
    }
    if (message.rotation !== 0) {
      writer.uint32(17).double(message.rotation);
    }
    if (message.x !== 0) {
      writer.uint32(25).double(message.x);
    }
    if (message.y !== 0) {
      writer.uint32(33).double(message.y);
    }
    if (message.id !== 0) {
      writer.uint32(40).uint32(message.id);
    }
    for (const v of message.children) {
      Attachment.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AttachedModule {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAttachedModule();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.moduleType = reader.int32() as any;
          continue;
        case 2:
          if (tag != 17) {
            break;
          }

          message.rotation = reader.double();
          continue;
        case 3:
          if (tag != 25) {
            break;
          }

          message.x = reader.double();
          continue;
        case 4:
          if (tag != 33) {
            break;
          }

          message.y = reader.double();
          continue;
        case 5:
          if (tag != 40) {
            break;
          }

          message.id = reader.uint32();
          continue;
        case 6:
          if (tag != 50) {
            break;
          }

          message.children.push(Attachment.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AttachedModule {
    return {
      moduleType: isSet(object.moduleType) ? moduleTypeFromJSON(object.moduleType) : 0,
      rotation: isSet(object.rotation) ? Number(object.rotation) : 0,
      x: isSet(object.x) ? Number(object.x) : 0,
      y: isSet(object.y) ? Number(object.y) : 0,
      id: isSet(object.id) ? Number(object.id) : 0,
      children: Array.isArray(object?.children) ? object.children.map((e: any) => Attachment.fromJSON(e)) : [],
    };
  },

  toJSON(message: AttachedModule): unknown {
    const obj: any = {};
    message.moduleType !== undefined && (obj.moduleType = moduleTypeToJSON(message.moduleType));
    message.rotation !== undefined && (obj.rotation = message.rotation);
    message.x !== undefined && (obj.x = message.x);
    message.y !== undefined && (obj.y = message.y);
    message.id !== undefined && (obj.id = Math.round(message.id));
    if (message.children) {
      obj.children = message.children.map((e) => e ? Attachment.toJSON(e) : undefined);
    } else {
      obj.children = [];
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AttachedModule>, I>>(base?: I): AttachedModule {
    return AttachedModule.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AttachedModule>, I>>(object: I): AttachedModule {
    const message = createBaseAttachedModule();
    message.moduleType = object.moduleType ?? 0;
    message.rotation = object.rotation ?? 0;
    message.x = object.x ?? 0;
    message.y = object.y ?? 0;
    message.id = object.id ?? 0;
    message.children = object.children?.map((e) => Attachment.fromPartial(e)) || [];
    return message;
  },
};

function createBaseAttachment(): Attachment {
  return { id: 0, slot: 0 };
}

export const Attachment = {
  encode(message: Attachment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.slot !== 0) {
      writer.uint32(16).uint32(message.slot);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Attachment {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAttachment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.id = reader.uint32();
          continue;
        case 2:
          if (tag != 16) {
            break;
          }

          message.slot = reader.uint32();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Attachment {
    return { id: isSet(object.id) ? Number(object.id) : 0, slot: isSet(object.slot) ? Number(object.slot) : 0 };
  },

  toJSON(message: Attachment): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.slot !== undefined && (obj.slot = Math.round(message.slot));
    return obj;
  },

  create<I extends Exact<DeepPartial<Attachment>, I>>(base?: I): Attachment {
    return Attachment.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Attachment>, I>>(object: I): Attachment {
    const message = createBaseAttachment();
    message.id = object.id ?? 0;
    message.slot = object.slot ?? 0;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
