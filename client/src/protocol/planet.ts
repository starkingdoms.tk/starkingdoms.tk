//@ts-nocheck
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "protocol.planet";

export enum PlanetType {
  UNKNOWN = 0,
  Earth = 1,
  Moon = 2,
  Mars = 3,
  UNRECOGNIZED = -1,
}

export function planetTypeFromJSON(object: any): PlanetType {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return PlanetType.UNKNOWN;
    case 1:
    case "Earth":
      return PlanetType.Earth;
    case 2:
    case "Moon":
      return PlanetType.Moon;
    case 3:
    case "Mars":
      return PlanetType.Mars;
    case -1:
    case "UNRECOGNIZED":
    default:
      return PlanetType.UNRECOGNIZED;
  }
}

export function planetTypeToJSON(object: PlanetType): string {
  switch (object) {
    case PlanetType.UNKNOWN:
      return "UNKNOWN";
    case PlanetType.Earth:
      return "Earth";
    case PlanetType.Moon:
      return "Moon";
    case PlanetType.Mars:
      return "Mars";
    case PlanetType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface Planet {
  /** Type of the planet */
  planetType: PlanetType;
  /** Translation on the X axis, in game units */
  x: number;
  /** Translation on the Y axis, in game units */
  y: number;
  /** The radius of the planet extending out from (x, y) */
  radius: number;
}

function createBasePlanet(): Planet {
  return { planetType: 0, x: 0, y: 0, radius: 0 };
}

export const Planet = {
  encode(message: Planet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.planetType !== 0) {
      writer.uint32(8).int32(message.planetType);
    }
    if (message.x !== 0) {
      writer.uint32(17).double(message.x);
    }
    if (message.y !== 0) {
      writer.uint32(25).double(message.y);
    }
    if (message.radius !== 0) {
      writer.uint32(33).double(message.radius);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Planet {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePlanet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.planetType = reader.int32() as any;
          continue;
        case 2:
          if (tag != 17) {
            break;
          }

          message.x = reader.double();
          continue;
        case 3:
          if (tag != 25) {
            break;
          }

          message.y = reader.double();
          continue;
        case 4:
          if (tag != 33) {
            break;
          }

          message.radius = reader.double();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Planet {
    return {
      planetType: isSet(object.planetType) ? planetTypeFromJSON(object.planetType) : 0,
      x: isSet(object.x) ? Number(object.x) : 0,
      y: isSet(object.y) ? Number(object.y) : 0,
      radius: isSet(object.radius) ? Number(object.radius) : 0,
    };
  },

  toJSON(message: Planet): unknown {
    const obj: any = {};
    message.planetType !== undefined && (obj.planetType = planetTypeToJSON(message.planetType));
    message.x !== undefined && (obj.x = message.x);
    message.y !== undefined && (obj.y = message.y);
    message.radius !== undefined && (obj.radius = message.radius);
    return obj;
  },

  create<I extends Exact<DeepPartial<Planet>, I>>(base?: I): Planet {
    return Planet.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Planet>, I>>(object: I): Planet {
    const message = createBasePlanet();
    message.planetType = object.planetType ?? 0;
    message.x = object.x ?? 0;
    message.y = object.y ?? 0;
    message.radius = object.radius ?? 0;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
