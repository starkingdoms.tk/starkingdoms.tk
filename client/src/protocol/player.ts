//@ts-nocheck
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "protocol.player";

export interface Player {
  /** The rotation, clockwise, in degrees, of the player */
  rotation: number;
  /** The translation on the X axis, in game units, of the player */
  x: number;
  /** The translation on the Y axis, in game units, of the player */
  y: number;
  /** The username of the player */
  username: string;
}

function createBasePlayer(): Player {
  return { rotation: 0, x: 0, y: 0, username: "" };
}

export const Player = {
  encode(message: Player, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.rotation !== 0) {
      writer.uint32(9).double(message.rotation);
    }
    if (message.x !== 0) {
      writer.uint32(17).double(message.x);
    }
    if (message.y !== 0) {
      writer.uint32(25).double(message.y);
    }
    if (message.username !== "") {
      writer.uint32(34).string(message.username);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Player {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePlayer();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 9) {
            break;
          }

          message.rotation = reader.double();
          continue;
        case 2:
          if (tag != 17) {
            break;
          }

          message.x = reader.double();
          continue;
        case 3:
          if (tag != 25) {
            break;
          }

          message.y = reader.double();
          continue;
        case 4:
          if (tag != 34) {
            break;
          }

          message.username = reader.string();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Player {
    return {
      rotation: isSet(object.rotation) ? Number(object.rotation) : 0,
      x: isSet(object.x) ? Number(object.x) : 0,
      y: isSet(object.y) ? Number(object.y) : 0,
      username: isSet(object.username) ? String(object.username) : "",
    };
  },

  toJSON(message: Player): unknown {
    const obj: any = {};
    message.rotation !== undefined && (obj.rotation = message.rotation);
    message.x !== undefined && (obj.x = message.x);
    message.y !== undefined && (obj.y = message.y);
    message.username !== undefined && (obj.username = message.username);
    return obj;
  },

  create<I extends Exact<DeepPartial<Player>, I>>(base?: I): Player {
    return Player.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Player>, I>>(object: I): Player {
    const message = createBasePlayer();
    message.rotation = object.rotation ?? 0;
    message.x = object.x ?? 0;
    message.y = object.y ?? 0;
    message.username = object.username ?? "";
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
