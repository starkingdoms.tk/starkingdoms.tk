//@ts-nocheck
import * as Long from "long";
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "protocol";

export interface PacketWrapper {
  /** What is the Packet ID of this packet? */
  packetId: number;
  /** Protobuf-encoded bytearray containing the actual packet */
  packetData: Uint8Array;
}

function createBasePacketWrapper(): PacketWrapper {
  return { packetId: 0, packetData: new Uint8Array() };
}

export const PacketWrapper = {
  encode(message: PacketWrapper, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.packetId !== 0) {
      writer.uint32(8).int64(message.packetId);
    }
    if (message.packetData.length !== 0) {
      writer.uint32(18).bytes(message.packetData);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PacketWrapper {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePacketWrapper();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag != 8) {
            break;
          }

          message.packetId = longToNumber(reader.int64() as Long);
          continue;
        case 2:
          if (tag != 18) {
            break;
          }

          message.packetData = reader.bytes();
          continue;
      }
      if ((tag & 7) == 4 || tag == 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): PacketWrapper {
    return {
      packetId: isSet(object.packetId) ? Number(object.packetId) : 0,
      packetData: isSet(object.packetData) ? bytesFromBase64(object.packetData) : new Uint8Array(),
    };
  },

  toJSON(message: PacketWrapper): unknown {
    const obj: any = {};
    message.packetId !== undefined && (obj.packetId = Math.round(message.packetId));
    message.packetData !== undefined &&
      (obj.packetData = base64FromBytes(message.packetData !== undefined ? message.packetData : new Uint8Array()));
    return obj;
  },

  create<I extends Exact<DeepPartial<PacketWrapper>, I>>(base?: I): PacketWrapper {
    return PacketWrapper.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<PacketWrapper>, I>>(object: I): PacketWrapper {
    const message = createBasePacketWrapper();
    message.packetId = object.packetId ?? 0;
    message.packetData = object.packetData ?? new Uint8Array();
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

function bytesFromBase64(b64: string): Uint8Array {
  if (tsProtoGlobalThis.Buffer) {
    return Uint8Array.from(tsProtoGlobalThis.Buffer.from(b64, "base64"));
  } else {
    const bin = tsProtoGlobalThis.atob(b64);
    const arr = new Uint8Array(bin.length);
    for (let i = 0; i < bin.length; ++i) {
      arr[i] = bin.charCodeAt(i);
    }
    return arr;
  }
}

function base64FromBytes(arr: Uint8Array): string {
  if (tsProtoGlobalThis.Buffer) {
    return tsProtoGlobalThis.Buffer.from(arr).toString("base64");
  } else {
    const bin: string[] = [];
    arr.forEach((byte) => {
      bin.push(String.fromCharCode(byte));
    });
    return tsProtoGlobalThis.btoa(bin.join(""));
  }
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
