//@ts-nocheck

export const protobufPackage = "protocol.state";

export enum State {
  UNKNOWN = 0,
  Handshake = 1,
  Play = 2,
  UNRECOGNIZED = -1,
}

export function stateFromJSON(object: any): State {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return State.UNKNOWN;
    case 1:
    case "Handshake":
      return State.Handshake;
    case 2:
    case "Play":
      return State.Play;
    case -1:
    case "UNRECOGNIZED":
    default:
      return State.UNRECOGNIZED;
  }
}

export function stateToJSON(object: State): string {
  switch (object) {
    case State.UNKNOWN:
      return "UNKNOWN";
    case State.Handshake:
      return "Handshake";
    case State.Play:
      return "Play";
    case State.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}
