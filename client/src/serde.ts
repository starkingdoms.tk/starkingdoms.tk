import {PacketWrapper} from "./protocol/starkingdoms-protocol";

export function encode(id: number, pktd: Uint8Array): Uint8Array {
    let pkt = PacketWrapper.encode({
        packetId: id,
        packetData: pktd
    }).finish();
    return pkt;
}

export function decode(pktd: Uint8Array): [number, Uint8Array] {
    let pkt = PacketWrapper.decode(pktd);
    return [pkt.packetId, pkt.packetData];
}