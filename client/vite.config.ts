import { defineConfig } from "vite";

export default defineConfig({
    build: {
        lib: {
            formats: ["es"],
            entry: {
                play: "play.html",
                index: "index.html",
            }
        },
        commonjsOptions: { include: [] },
    },
    optimizeDeps: {
        disabled: false
    }
});
