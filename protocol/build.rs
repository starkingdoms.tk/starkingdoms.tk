fn main() {
    protobuf_codegen::Codegen::new()
        .cargo_out_dir("protos")
        .include("src/pbuf")
        .input("src/pbuf/starkingdoms-protocol.proto")
        .input("src/pbuf/message_c2s.proto")
        .input("src/pbuf/message_s2c.proto")
        .input("src/pbuf/planet.proto")
        .input("src/pbuf/player.proto")
        .input("src/pbuf/state.proto")
        .input("src/pbuf/goodbye_reason.proto")
        .input("src/pbuf/module.proto")
        .input("src/pbuf/input.proto")
        .run_from_script();
}
