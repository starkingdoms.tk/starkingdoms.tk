use serde::{Deserialize, Serialize};

// ALL FIELDS **MUST** BE WRAPPED IN Option<>
#[derive(Serialize, Deserialize, Default, Clone, Debug)]
pub struct APISavedPlayerData {}
