use serde::{Deserialize, Serialize};

pub const PROTOCOL_VERSION: u32 = 1;

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum State {
    Handshake,
    Play,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum MessageC2S {
    Hello {
        version: u32,
        requested_username: String,
        next_state: State,
    },

    Goodbye {
        reason: GoodbyeReason,
    },

    Chat {
        message: String,
    },

    Ping {},
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum MessageS2C {
    Hello {
        version: u32,
        given_username: String,
        next_state: State,
    },

    Goodbye {
        reason: GoodbyeReason,
    },

    Chat {
        from: String,
        message: String,
    },

    Pong {},

    PlayersUpdate {
        players: Vec<ProtocolPlayer>,
    },

    PlanetData {
        planets: Vec<ProtocolPlanet>,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ProtocolPlayer {
    pub rotation: f64,
    pub x: f64,
    pub y: f64,
    pub username: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum GoodbyeReason {
    UnsupportedProtocol { supported: u32, got: u32 },
    UnexpectedPacket,
    UnexpectedNextState,
    UsernameTaken,
    PingPongTimeout,
    Done,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ProtocolPlanet {
    pub planet_type: PlanetType,
    pub x: f64,
    pub y: f64,
    pub radius: f64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum PlanetType {
    Earth,
}

impl PlanetType {
    pub fn as_texture_id(&self) -> String {
        match self {
            PlanetType::Earth => "earth".to_string(),
        }
    }
}

pub fn pc2s(pkt: &MessageC2S) -> Vec<u8> {
    rmp_serde::to_vec(pkt).unwrap()
}
pub fn ps2c(pkt: &MessageS2C) -> Vec<u8> {
    rmp_serde::to_vec(pkt).unwrap()
}
