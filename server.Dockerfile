FROM ghcr.io/void-linux/void-linux:latest-full-x86_64

RUN xbps-install -S && xbps-install -f base-files && xbps-install -Syyff gcc

COPY target/release/starkingdoms-server /bin/starkingdoms-server

CMD ["/bin/starkingdoms-server"]