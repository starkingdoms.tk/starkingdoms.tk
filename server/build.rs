use cargo_metadata::MetadataCommand;
use std::process::Command;

fn main() {
    let path = std::env::var("CARGO_MANIFEST_DIR").unwrap();

    let meta = MetadataCommand::new()
        .manifest_path("./Cargo.toml")
        .current_dir(&path)
        .exec()
        .unwrap();

    let root = meta.root_package().unwrap();

    let version = root.version.to_string();
    let version_name = root.metadata["version-name"].to_string().replace('"', "");
    let description = root.metadata["slp-description"]
        .to_string()
        .replace('"', "");

    let output = Command::new("git")
        .args(["rev-parse", "--short", "HEAD"])
        .output()
        .unwrap();
    let git_hash = String::from_utf8(output.stdout).unwrap();

    println!("cargo:rustc-env=STK_VERSION={}", version);
    println!("cargo:rustc-env=STK_VERSION_NAME={}", version_name);
    println!("cargo:rustc-env=STK_SLP_DESCRIPTION={}", description);
    println!(
        "cargo:rustc-env=STK_CHANNEL={}",
        std::env::var("STK_CHANNEL").unwrap_or("dev".to_string())
    );
    println!(
        "cargo:rustc-env=STK_BUILD={}-{}-{}",
        std::env::var("STK_CHANNEL").unwrap_or("dev".to_string()),
        std::env::var("STK_BUILD_NUM").unwrap_or("local".to_string()),
        git_hash
    );
    println!("cargo:rerun-if-changed=Cargo.toml");
    println!("cargo:rerun-if-env-changed=STK_BUILD_NUM");
}
