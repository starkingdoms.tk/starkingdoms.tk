use tungstenite::Message;

#[macro_export]
macro_rules! send {
    ($writer:expr,$pkt:expr) => {
        $writer.send($crate::macros::_generic_pkt_into($pkt))
    };
}

pub fn _generic_pkt_into(p: Vec<u8>) -> Message {
    Message::from(p)
}

#[macro_export]
macro_rules! recv {
    ($reader:expr) => {{
        if let Some(future_result) = $reader.next().now_or_never() {
            if let Some(msg) = future_result {
                match msg {
                    Ok(msg) => {
                        if msg.is_binary() {
                            match MessageC2S::try_from(msg.into_data().as_slice()) {
                                Ok(d) => Ok(Some(d)),
                                Err(e) => {
                                    log::error!("error deserializing message: {}", e);
                                    Ok(None)
                                }
                            }
                        } else {
                            Ok(None)
                        }
                    }
                    Err(e) => {
                        log::error!("error receiving message: {}", e);
                        Ok(None)
                    }
                }
            } else {
                log::error!("pipe closed");
                Err("Pipe closed")
            }
        } else {
            Ok(None)
        }
    }};
}

#[macro_export]
macro_rules! recv_now {
    ($reader:expr) => {{
        if let Some(msg) = $reader.next().await {
            match msg {
                Ok(msg) => {
                    if msg.is_binary() {
                        match MessageC2S::try_from(&msg.into_data()) {
                            Ok(d) => Ok(Some(d)),
                            Err(e) => {
                                log::error!("error deserializing message: {}", e);
                                Ok(None)
                            }
                        }
                    } else {
                        Ok(None)
                    }
                }
                Err(e) => {
                    log::error!("error receiving message: {}", e);
                    Ok(None)
                }
            }
        } else {
            log::error!("pipe closed");
            Err("Pipe closed")
        }
    }};
}
