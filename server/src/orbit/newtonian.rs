use crate::orbit::kepler::kepler_equation;

pub const NEWTONIAN_STEP_SIZE: f64 = 0.0001;
pub const NEWTONIAN_ACCEPTABLE_ERROR: f64 = 0.000_000_01;

pub fn solve_kepler_with_newtonian(
    mean_anomaly: f64,
    eccentricity: f64,
    max_iterations: u64,
) -> f64 {
    let mut guess = mean_anomaly;

    for _ in 0..max_iterations {
        let y = kepler_equation(guess, mean_anomaly, eccentricity);

        // exit early if output of function is very close to zero
        if y.abs() < NEWTONIAN_ACCEPTABLE_ERROR {
            break;
        }

        // otherwise, update guess
        let slope = (kepler_equation(guess + NEWTONIAN_STEP_SIZE, mean_anomaly, eccentricity) - y)
            / NEWTONIAN_STEP_SIZE;
        let step = y / slope;
        guess -= step;
    }

    guess
}
