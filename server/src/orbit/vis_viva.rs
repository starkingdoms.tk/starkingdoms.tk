pub fn vis_viva(
    distance_between_centers: f64,
    semi_major: f64,
    g: f64,
    mass_of_bigger: f64,
) -> f64 {
    (g * mass_of_bigger * (2.0 / distance_between_centers - 1.0 / semi_major)).sqrt()
}
