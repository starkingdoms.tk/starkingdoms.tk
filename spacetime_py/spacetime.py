import sys
from ninja_syntax import Writer
import os


def scan_assets(build_root):
    print(f'[spacetime] Scanning {build_root}/assets/src for assets')
    assets = []
    for entry in os.scandir(f'{build_root}/assets/src'):
        if entry.is_file() and entry.name.endswith('.ink.svg'):
            assets.append(f'{entry.name}')
    print(f'[spacetime] Found {len(assets)} assets')
    return assets


default_asset_size = 512
asset_override = {
    'earth.ink.svg': 2048,
    'moon.ink.svg': 2048
}

def gen_inkscape_rules_for_asset_size(size, writer):
    writer.rule(f'inkscape_{size}px_full', f'inkscape -w {size * 1} -h {size * 1} $in -o $out')
    writer.rule(f'inkscape_{size}px_375', f'inkscape -w {int(size * 0.375)} -h {int(size * 0.375)} $in -o $out')
    writer.rule(f'inkscape_{size}px_125', f'inkscape -w {int(size * 0.125)} -h {int(size * 0.125)} $in -o $out')


def gen_inkscape_rules_for_asset_sizes(writer):
    built_sizes_for = []
    gen_inkscape_rules_for_asset_size(default_asset_size, writer)
    built_sizes_for.append(default_asset_size)
    for override in asset_override:
        if not asset_override[override] in built_sizes_for:
            gen_inkscape_rules_for_asset_size(asset_override[override], writer)
            built_sizes_for.append(asset_override[override])


def asset_size(asset):
    if asset.split('/')[-1] in asset_override:
        return asset_override[asset.split('/')[-1]]
    else:
        return default_asset_size


def gen_inkscape_rules_for_asset(root, asset, writer, files_375, files_full, files_125):
    in_file = f'{root}/assets/src/{asset}'
    out_file_name = asset.split('.')[0].split('/')[-1]

    out_full = f'{root}/assets/final/full/{out_file_name}.png'
    files_full.append(out_full)
    rule_full = f'inkscape_{asset_size(asset)}px_full'

    out_375 = f'{root}/assets/final/375/{out_file_name}.png'
    files_375.append(out_375)
    rule_375 = f'inkscape_{asset_size(asset)}px_375'

    out_125 = f'{root}/assets/final/125/{out_file_name}.png'
    files_125.append(out_125)
    rule_125 = f'inkscape_{asset_size(asset)}px_125'

    writer.build([out_full], rule_full, [in_file])
    writer.build([out_375], rule_375, [in_file])
    writer.build([out_125], rule_125, [in_file])

def gen_rules_for_server(root, env, writer, modules):
    if env == 'dev':
        out_dir = 'debug'
        writer.rule('cargo-server', f'cargo build --bin starkingdoms-server --features "{modules}"',
                    depfile=f'{root}/target/debug/starkingdoms-server.d', pool='console')
    elif env == 'prod':
        out_dir = 'release'
        writer.rule('cargo-server', f'cargo build --bin starkingdoms-server --release --features "{modules}"',
                    depfile=f'{root}/target/release/starkingdoms-server.d', pool='console')

    writer.build([f'{root}/target/{out_dir}/starkingdoms-server'], 'cargo-server', ['server/Cargo.toml'])
    writer.build(['server'], 'phony', [f'{root}/target/{out_dir}/starkingdoms-server'])

def gen_rules_for_api(root, env, writer, modules):
    if env == 'dev':
        out_dir = 'debug'
        writer.rule('cargo-api', f'cargo build --bin starkingdoms-api --features "{modules}"',
                    depfile=f'{root}/target/debug/starkingdoms-api.d', pool='console')
    elif env == 'prod':
        out_dir = 'release'
        writer.rule('cargo-api', f'cargo build --bin starkingdoms-api --release --features "{modules}"',
                    depfile=f'{root}/target/release/starkingdoms-api.d', pool='console')

    writer.build([f'{root}/target/{out_dir}/starkingdoms-api'], 'cargo-api', ['server/Cargo.toml'])
    writer.build(['api'], 'phony', [f'{root}/target/{out_dir}/starkingdoms-api'])

def gen_inkscape(root, assets, writer, files_375, files_full, files_125):
    gen_inkscape_rules_for_asset_sizes(writer)

    for asset in assets:
        gen_inkscape_rules_for_asset(root, asset, writer, files_375, files_full, files_125)


def gen_packers(root, writer, files_375, files_full, files_125):
    # sheep pack assets/final/full/*.png -f amethyst_named -o assets/dist/spritesheet-full
    writer.rule(f'pack', 'cd assets/dist && atlasify -m 4096,4096 -o $out $in && touch $out')

    writer.build(f'{root}/assets/dist/spritesheet-full', 'pack', inputs=files_full,
                 implicit_outputs=[f'{root}/assets/dist/spritesheet-full.png',
                                   f'{root}/assets/dist/spritesheet-full.json'])
    writer.build(f'asset-full', 'phony', inputs=[f'{root}/assets/dist/spritesheet-full'])

    writer.build(f'{root}/assets/dist/spritesheet-375', 'pack', inputs=files_375,
                 implicit_outputs=[f'{root}/assets/dist/spritesheet-375.png',
                                   f'{root}/assets/dist/spritesheet-375.json'])
    writer.build(f'asset-375', 'phony', inputs=[f'{root}/assets/dist/spritesheet-375'])

    writer.build(f'{root}/assets/dist/spritesheet-125', 'pack', inputs=files_125,
                 implicit_outputs=[f'{root}/assets/dist/spritesheet-125.png',
                                   f'{root}/assets/dist/spritesheet-125.json'])
    writer.build(f'asset-125', 'phony', inputs=[f'{root}/assets/dist/spritesheet-125'])

    writer.build(f'asset', 'phony',
                 inputs=[f'{root}/assets/dist/spritesheet-full', f'{root}/assets/dist/spritesheet-375',
                         f'{root}/assets/dist/spritesheet-125'])


def generate_assets_build_command(root, assets, writer):
    files_full = []
    files_375 = []
    files_125 = []
    gen_inkscape(root, assets, writer, files_375, files_full, files_125)
    gen_packers(root, writer, files_375, files_full, files_125)


def main():
    target = sys.argv[1]
    env = sys.argv[2]
    root = sys.argv[3]
    if len(sys.argv) > 5:
        modules = sys.argv[5]
    else:
        modules = ""

    verbose = False
    if len(sys.argv) > 4:
        if sys.argv[4] == '-v':
            verbose = True

    print(f'[spacetime] Configuring ninja for PRIMARY_TARGET={target} with ENV={env}, BUILDROOT={root}, MODULES={modules}')

    with open(f'{root}/build.ninja', 'w') as f:
        writer = Writer(f)

        writer.comment('Generated by spacetime.py')
        writer.comment('Do not manually edit this file')

        if target == 'client' or target == 'asset':
            assets = scan_assets(root)
            if verbose:
                print(f'[spacetime -v] discovered assets: {assets}')
            generate_assets_build_command(root, assets, writer)
        elif target == 'server':
            gen_rules_for_server(root, env, writer, modules)
        elif target == 'api':
            gen_rules_for_api(root, env, writer, modules)

    print(f'[spacetime] Configured build')


if __name__ == "__main__":
    main()
