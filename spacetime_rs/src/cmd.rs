use which::which;

pub fn enforce_commands() {
    println!("[spacetime] checking for required tooling");
    _enforce_command("cargo");
    _enforce_command("ninja");
    _enforce_command("yarn");
    _enforce_command("inkscape");
    _enforce_command("atlasify");
    println!("[spacetime] all required tools present");
}

fn _enforce_command(cmd: &str) {
    if which(cmd).is_err() {
        eprintln!(
            "[!] Unable to find required binary {}. Please install it to continue.",
            cmd
        );
    }
}
