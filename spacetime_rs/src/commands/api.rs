use crate::configure::create_writer;
use crate::configure::rust::configure_rust_target;
use crate::ninja::{exec, exec_ninja};
use std::error::Error;
use std::path::PathBuf;

pub fn build_api(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("api", "dev", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["api".to_string()])?;

    Ok(())
}

pub fn build_api_prod(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("api", "prod", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["api".to_string()])?;

    Ok(())
}

pub fn run_api(args: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("api", "dev", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["api".to_string()])?;

    exec(
        root.join("target/debug/starkingdoms-api").to_str().unwrap(),
        &root.join("api"),
        args,
    )?;

    Ok(())
}

pub fn run_api_prod(args: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("api", "prod", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["api".to_string()])?;

    exec(
        root.join("target/release/starkingdoms-api")
            .to_str()
            .unwrap(),
        &root.join("api"),
        args,
    )?;

    Ok(())
}
