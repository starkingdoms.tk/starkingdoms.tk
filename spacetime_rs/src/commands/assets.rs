use crate::configure::asset::configure_assets;
use crate::configure::create_writer;
use crate::ninja::exec_ninja;
use std::error::Error;
use std::path::PathBuf;
use std::time::SystemTime;

pub fn build_assets(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    let start = SystemTime::now();

    configure_assets(&mut config_file_writer, &root)?;

    let end = SystemTime::now();
    let duration = end.duration_since(start).unwrap();

    println!(
        "[spacetime] configure completed in {} seconds",
        duration.as_secs_f32()
    );

    exec_ninja(&root, vec!["asset".to_string()])?;

    Ok(())
}
