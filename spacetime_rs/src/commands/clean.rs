use crate::ninja::exec;
use std::error::Error;
use std::fs;
use std::path::PathBuf;

pub fn clean(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    exec("cargo", &root, vec!["clean".to_string()])?;
    fs::remove_dir_all(root.join("assets").join("dist"))?;
    fs::remove_dir_all(root.join("assets").join("final"))?;
    Ok(())
}
