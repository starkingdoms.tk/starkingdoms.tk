use crate::configure::client::configure_client;
use crate::configure::create_writer;
use crate::ninja::{exec, exec_ninja};
use std::error::Error;
use std::path::PathBuf;

pub fn run_http(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_client(&mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["asset".to_string()])?;

    exec("yarn", &root.join("client"), vec![])?;
    exec(
        "yarn",
        &root.join("client"),
        vec!["run".to_string(), "dev".to_string()],
    )?;

    Ok(())
}

pub fn client_protobuf(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    exec("yarn", &root.join("client"), vec![])?;
    exec("yarn", &root.join("client"), vec!["protobuf".to_string()])
}

pub fn build_client_prod(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_client(&mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["asset".to_string()])?;

    exec("yarn", &root.join("client"), vec![])?;
    exec("yarn", &root.join("client"), vec!["build".to_string()])
}
