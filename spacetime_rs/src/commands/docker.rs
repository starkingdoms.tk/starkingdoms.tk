use crate::commands::api::build_api_prod;
use crate::commands::client::build_client_prod;
use crate::commands::server::build_server_prod;
use crate::ninja::exec;
use sedregex::find_and_replace;
use std::error::Error;
use std::fs;
use std::path::PathBuf;
use std::process::Command;

fn _build(img: &str, channel: &str, root: &PathBuf) -> Result<(), Box<dyn Error>> {
    let mut patched = "".to_string();
    // compile the various thingies
    if img == "server" {
        build_server_prod(vec![], root.clone())?;
    } else if img == "api" {
        build_api_prod(vec![], root.clone())?;
    } else if img == "web" {
        let (a, b) = match channel {
            "stable" => ("starkingdoms", "tk"),
            _ => (channel, "starkingdoms.io"),
        };

        let index_html_path = root.clone().join("client/").join("index.html");

        let index_html_src = fs::read_to_string(&index_html_path)?;
        let index_html_patched = find_and_replace(&index_html_src, &[
            format!("s/let api_server = \"http:\\/\\/localhost:8080\";/let api_server = \"https:\\/\\/api.{}.{}\";/", a, b)
        ])?;

        fs::write(&index_html_path, index_html_patched.as_bytes())?;

        fs::write(
            root.clone().join("client/").join("index.html.orig"),
            index_html_src.clone(),
        )?;

        patched = index_html_src.clone();

        build_client_prod(vec![], root.clone())?
    }

    let git_commit_id = String::from_utf8(
        Command::new("git")
            .args(["rev-parse", "--short", "HEAD"])
            .current_dir(root)
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap()
    .replace('\n', "");
    exec(
        "docker",
        root,
        vec![
            "buildx",
            "build",
            "-f",
            root.join(format!("{}.Dockerfile", img)).to_str().unwrap(),
            "-t",
            &format!(
                "registry.gitlab.com/starkingdoms.tk/starkingdoms.tk:{}-{}",
                img, git_commit_id
            ),
            root.to_str().unwrap(),
        ]
        .iter()
        .map(|u| u.to_string())
        .collect(),
    )?;
    exec(
        "docker",
        root,
        vec![
            "buildx",
            "build",
            "-f",
            root.join(format!("{}.Dockerfile", img)).to_str().unwrap(),
            "-t",
            &format!(
                "registry.gitlab.com/starkingdoms.tk/starkingdoms.tk:{}-{}",
                img, channel
            ),
            root.to_str().unwrap(),
        ]
        .iter()
        .map(|u| u.to_string())
        .collect(),
    )?;

    exec(
        "docker",
        root,
        vec![
            "push",
            &format!(
                "registry.gitlab.com/starkingdoms.tk/starkingdoms.tk:{}-{}",
                img, git_commit_id
            ),
        ]
        .iter()
        .map(|u| u.to_string())
        .collect(),
    )?;
    exec(
        "docker",
        root,
        vec![
            "push",
            &format!(
                "registry.gitlab.com/starkingdoms.tk/starkingdoms.tk:{}-{}",
                img, channel
            ),
        ]
        .iter()
        .map(|u| u.to_string())
        .collect(),
    )?;

    if img == "web" {
        fs::write(root.clone().join("client/").join("index.html"), patched)?;
    }

    Ok(())
}

pub fn build_docker_api(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("api", "bleeding", &root)
}
pub fn build_docker_server(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("server", "bleeding", &root)
}
pub fn build_docker_web(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("web", "bleeding", &root)
}
pub fn build_docker(_a: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    build_docker_api(_a.clone(), root.clone())?;
    build_docker_server(_a.clone(), root.clone())?;
    build_docker_web(_a, root)
}

pub fn build_docker_api_beta(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("api", "beta", &root)
}
pub fn build_docker_server_beta(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("server", "beta", &root)
}
pub fn build_docker_web_beta(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("web", "beta", &root)
}
pub fn build_docker_beta(_a: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    build_docker_api_beta(_a.clone(), root.clone())?;
    build_docker_server_beta(_a.clone(), root.clone())?;
    build_docker_web_beta(_a, root)
}

pub fn build_docker_api_stable(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("api", "stable", &root)
}
pub fn build_docker_server_stable(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("server", "stable", &root)
}
pub fn build_docker_web_stable(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    _build("web", "stable", &root)
}
pub fn build_docker_stable(_a: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    build_docker_api_stable(_a.clone(), root.clone())?;
    build_docker_server_stable(_a.clone(), root.clone())?;
    build_docker_web_stable(_a, root)
}
