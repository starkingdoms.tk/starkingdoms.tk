use crate::configure::create_writer;
use crate::configure::rust::configure_rust_target;
use crate::ninja::{exec, exec_ninja};
use std::error::Error;
use std::path::PathBuf;

pub fn build_server(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("server", "dev", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["server".to_string()])?;

    Ok(())
}

pub fn build_server_prod(_: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("server", "prod", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["server".to_string()])?;

    Ok(())
}

pub fn run_server(args: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("server", "dev", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["server".to_string()])?;

    exec(
        root.join("target/debug/starkingdoms-server")
            .to_str()
            .unwrap(),
        &root,
        args,
    )?;

    Ok(())
}

pub fn run_server_prod(args: Vec<String>, root: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut config_file_writer = create_writer(&root)?;

    configure_rust_target("server", "prod", &mut config_file_writer, &root)?;

    exec_ninja(&root, vec!["server".to_string()])?;

    exec(
        root.join("target/release/starkingdoms-server")
            .to_str()
            .unwrap(),
        &root,
        args,
    )?;

    Ok(())
}
