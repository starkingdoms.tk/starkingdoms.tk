pub const ASSET_DIR: &str = "assets/";
pub const ASSETS_DIST_SUBDIR: &str = "dist/";
pub const ASSETS_SRC_SUBDIR: &str = "src/";
pub const ASSETS_FINAL_SUBDIR: &str = "final/";
