use crate::configure::asset::configure_assets;
use crate::ninja::NinjaWriter;
use std::error::Error;
use std::fs::File;
use std::path::Path;

pub fn configure_client(writer: &mut NinjaWriter<File>, root: &Path) -> Result<(), Box<dyn Error>> {
    configure_assets(writer, root)?;

    Ok(())
}
