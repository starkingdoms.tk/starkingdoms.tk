/*
def gen_rules_for_api(root, env, writer, modules):
    if env == 'dev':
        out_dir = 'debug'
        writer.rule('cargo-api', f'cargo build --bin starkingdoms-api --features "{modules}"',
                    depfile=f'{root}/target/debug/starkingdoms-api.d', pool='console')
    elif env == 'prod':
        out_dir = 'release'
        writer.rule('cargo-api', f'cargo build --bin starkingdoms-api --release --features "{modules}"',
                    depfile=f'{root}/target/release/starkingdoms-api.d', pool='console')

    writer.build([f'{root}/target/{out_dir}/starkingdoms-api'], 'cargo-api', ['server/Cargo.toml'])
    writer.build(['api'], 'phony', [f'{root}/target/{out_dir}/starkingdoms-api'])
 */

use crate::ninja::NinjaWriter;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::path::Path;

pub fn configure_rust_target(
    rust_target: &str,
    rust_env: &str,
    writer: &mut NinjaWriter<File>,
    root: &Path,
) -> Result<(), Box<dyn Error>> {
    let out_dir;
    if rust_env == "dev" {
        out_dir = "debug";
        writer.rule(
            &format!("cargo-{}", rust_target),
            &format!("cargo build --bin starkingdoms-{}", rust_target),
            None,
            Some(
                root.join("target/debug/")
                    .join(format!("starkingdoms-{}.d", rust_target))
                    .to_str()
                    .unwrap(),
            ),
            None,
            Some("console"),
            None,
            None,
            None,
            None,
        )?;
    } else {
        out_dir = "release";
        writer.rule(
            &format!("cargo-{}", rust_target),
            &format!("cargo build --bin starkingdoms-{} --release", rust_target),
            None,
            Some(
                root.join("target/release/")
                    .join(format!("starkingdoms-{}.d", rust_target))
                    .to_str()
                    .unwrap(),
            ),
            None,
            Some("console"),
            None,
            None,
            None,
            None,
        )?;
    }

    writer.build(
        vec![root
            .join(format!("target/{}/", out_dir))
            .join(format!("starkingdoms-{}", rust_target))
            .to_str()
            .unwrap()
            .to_string()],
        format!("cargo-{}", rust_target),
        vec![root.join("server/Cargo.toml").to_str().unwrap().to_string()],
        vec![],
        vec![],
        HashMap::new(),
        vec![],
        None,
        None,
    )?;
    writer.build(
        vec![rust_target.to_string()],
        "phony".to_string(),
        vec![root
            .join(format!("target/{}/", out_dir))
            .join(format!("starkingdoms-{}", rust_target))
            .to_str()
            .unwrap()
            .to_string()],
        vec![],
        vec![],
        HashMap::new(),
        vec![],
        None,
        None,
    )?;

    Ok(())
}
