FROM node

RUN npm i -g parcel

COPY client /client

RUN cd /client && yarn
RUN cd /client && yarn build

FROM nginx

COPY --from=0 /client/dist /usr/share/nginx/html
COPY assets/ /usr/share/nginx/html/assets
COPY docker/mime-types.conf /etc/nginx/conf.d/mime-types.conf